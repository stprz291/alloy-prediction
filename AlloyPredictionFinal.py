import tkinter as tk
import numpy as np
from tkinter import messagebox
import pandas as pd
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import MinMaxScaler

# Load the data
df = pd.read_csv('output2.csv')

# Replace "Not available" with NaN and drop rows with NaN values
df = df.replace("Not available", np.nan).dropna()

# Split the data into features and target
X = df[['TensileStrength', 'YoungModulus', 'ThermalConductivity', 'MeltingPoint', 'Density']]
y = df['AlloyName']

# Scale the features
scaler = MinMaxScaler()
X = scaler.fit_transform(X)

# Train the model
model = KNeighborsClassifier(n_neighbors=5)
model.fit(X, y)

def predict_alloy(k=3):
    # Get user input from input fields
    user_input = [e.get() for e in entries]
    
    # Check if any input fields are empty
    if not all(user_input):
        messagebox.showerror(title="Invalid Input", message="Please enter numerical values for all input fields.")
        return
    
    # Check if input fields contain only numbers
    for input_val in user_input:
        if not input_val.replace(".", "").isdigit():
            messagebox.showerror(title="Invalid Input", message="Only numbers allowed in the input field.")
            return
        
    # Convert input values to float
    user_input = [float(e) for e in user_input]

    # Find the k nearest neighbors of the user input
    distances, indices = model.kneighbors([user_input], n_neighbors=k)

    # Extract the corresponding alloy names
    predicted_alloys = [y[i] for i in indices[0]]

    # Display the predicted alloys in the label widget
    text = "The predicted alloys are:\n" + "\n".join(predicted_alloys)
    prediction_label.config(text=text, anchor='center')


# Create the GUI
root = tk.Tk()
root.title("PWC Alloy Predictor")
width = root.winfo_screenwidth()
height = root.winfo_screenheight()
root.geometry(f"{width}x{height}")
UI_label = tk.Label(root, text='PWC',font=("Arial", 60),fg="Orange",anchor="center")
UI_label.place(x=300, y=55)
UI_label = tk.Label(root, text='Alloy',font=("Arial", 60),fg="#FFBF00",anchor="center")
UI_label.place(x=520, y=55)
UI_label = tk.Label(root, text='Predictor',font=("Arial", 60),fg="#800000",anchor="center")
UI_label.place(x=720, y=55)

# Create the entry fields
entries = []
label= "Tensile Strength (MPa)"
label_text1 = tk.Label(root, text=label)
label= "Young's Modulus (GPa)"
label_text2 = tk.Label(root, text=label)
label= 'Thermal Conductivity (W/m-K) '
label_text3 = tk.Label(root, text=label)
label= 'Melting Point (°C)'
label_text4 = tk.Label(root, text=label)
label= 'Density (g/cm³)'
label_text5 = tk.Label(root, text=label)
label_text1.place(x=530, y=190)
entry1 = tk.Entry(root)
entry1= tk.Entry(root, width=20)
entry1.pack()
entry1.place(x=750, y=190)
entries.append(entry1)
label_text2.place(x=530, y=235)
entry2 = tk.Entry(root)
entry2= tk.Entry(root, width=20)
entry2.pack()
entry2.place(x=750, y=235)
entries.append(entry2)
label_text3.place(x=530, y=285)
entry3 = tk.Entry(root)
entry3= tk.Entry(root, width=20)
entry3.pack()
entry3.place(x=750, y=285)
entries.append(entry3)
label_text4.place(x=530, y=335)
entry4 = tk.Entry(root)
entry4= tk.Entry(root, width=20)
entry4.pack()
entry4.place(x=750, y=335)
entries.append(entry4)
label_text5.place(x=530, y=385)
entry5 = tk.Entry(root)
entry5= tk.Entry(root, width=20)
entry5.pack()
entry5.place(x=750, y=385)
entries.append(entry5)


# Create the predict button
predict_button = tk.Button(root, text="Predict", command=predict_alloy)
predict_button.place(x=680, y=475)
prediction_label = tk.Label(root, text="", font=("Arial", 20))
prediction_label.place(x=1, y=565)
# Run the GUI
root.mainloop()
